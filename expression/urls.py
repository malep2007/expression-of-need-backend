from typing import List
from django.urls import path


from expression.views import expression_dashboard, expression_list

urlpatterns = [
    path('', expression_dashboard, name='dashboard'),
    path('list/', expression_list, name='expression-list'),
]