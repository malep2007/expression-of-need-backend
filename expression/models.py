import datetime
from django.db.models.base import Model
from django.utils import tree
from user.models import Staff
from django.db import models
from django.utils import timezone

validation_choices = [
    ("HoD Validation", "HoD"),
    ("HoS Validation", "HoS"),
    ("Final Validation", "Final"),
    ("No Validation", "None")
]

expression_status = [
    ("Open", "O"),
    ("Closed", "C"),
    ("New", "N")
]

budget_choices = [
    ("CAPEX", "C"),
    ("OPEX", "O")
]


class Expression(models.Model):
    reason = models.CharField(max_length=123, null=True)
    location = models.CharField(max_length=123, null=True)
    what = models.CharField(max_length=123, null=True)
    validation = models.CharField(max_length=20, choices=validation_choices, default="No Validation")
    status = models.CharField(max_length=20, choices=expression_status, default="New")
    recommended_suppliers = models.TextField()
    issuer = models.ForeignKey(Staff, on_delete=models.SET_NULL, null=True)
    delivery_date = models.DateField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    comment = models.ForeignKey("ExpressionComment", null=True, on_delete=models.SET_NULL)
    budget_estimate = models.IntegerField(default=0, null=True)
    budget_type = models.CharField(max_length=20, choices=budget_choices, default="CAPEX")
    quantity = models.IntegerField(default=0, null=True)
    unit_of_measurement = models.CharField(max_length=10, null=True)
    file_url = models.URLField(null=True)
    staff_assigned = models.ForeignKey(to=Staff, on_delete=models.SET_NULL, null=True, related_name='staff_assigned')

    class Meta:
        ordering = ['-created_at', ]


class ExpressionComment(models.Model):
    comment = models.CharField(max_length=123, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(null=True)

    def __str__(self) -> str:
        return f'{self.comment}'
