from typing import List
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns


from expression.views import CreateExpression, GetExpressionsListView

urlpatterns = format_suffix_patterns([
    path('', GetExpressionsListView.as_view()),
    path('create/', CreateExpression.as_view())
])