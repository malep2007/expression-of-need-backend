from rest_framework import generics
from user.serializers import StaffSerializer
from user.models import Staff
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from expression.serializers import ExpressionSerializer
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rest_framework import authentication, permissions, request, serializers, status
from rest_framework.pagination import PageNumberPagination

from expression.models import Expression


class GetExpressionsListView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]
    pagination_class = [PageNumberPagination]

    serializer_class = ExpressionSerializer

    def get_object(self, user):
        try:
            procurement = get_object_or_404(Group, name='Procurement')
            users = procurement.user_set.all()
            user = User.objects.get(username=user.username)
            if user in users:
                return Expression.objects.all()
            else:
                return Expression.objects.filter(issuer__user__username=user.username)
        except Exception as ex:
            return ex

    def get(self, request, *args, **kwargs):
        try:
            user = request.user
            expressions = self.get_object(user)
            serializer = ExpressionSerializer(expressions, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as ex:
            return Response({"error": f'{ex}'}, status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    def put(self, request,  *args, **kwargs):
        try:
            expression = Expression.objects.get(id=request.data.pop('id'))
            serializer = ExpressionSerializer(expression, data=request.data, many=False, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as ex:
            return Response({"error": f'{ex}'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CreateExpression(APIView):
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def post(self, request):
        try:
            serializer = ExpressionSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as ex:
            return Response({"error": f"{ex}"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
