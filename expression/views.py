from django.shortcuts import render, HttpResponse
from django.contrib.auth.decorators import login_required

@login_required
def expression_dashboard(request):
    return render(
        request, 
        template_name='expression/dashboard.html', 
        context={
            'message':'NOT YET IMPLEMENTED', 
            'title': 'Expression List', 
            'user': request.user
            }
        )

def expression_list(request):
    return render(request, template_name='expression/expression_list.html')