"""
Serializer module for the user app
"""
from user.serializers import StaffSerializer
from user.models import Staff
from django.contrib.auth.models import User
from django.utils import tree
from rest_framework import serializers, permissions

import datetime

from expression.models import Expression, ExpressionComment


class ExpressionCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExpressionComment
        fields = ['comment', 'date_created', 'date_updated']

    def create(self, validated_data):
        comment = validated_data.pop('comment')
        date_created = validated_data.pop('date_created')
        date_updated = None
        if validated_data.get('date_updated'):
            date_updated = validated_data.pop('date_updated')
        else:
            date_updated = date_created
        instance = ExpressionComment.objects.create(comment=comment, date_created=date_created, date_updated=date_updated)
        return instance


class ExpressionSerializer(serializers.ModelSerializer):
    issuer = StaffSerializer()
    staff_assigned = StaffSerializer(required=False)
    delivery_date = serializers.DateField(format="%Y-%m-%d")
    updated_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)
    created_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)

    class Meta:
        model = Expression
        fields = [
            'id',
            'reason',
            'location',
            'what',
            'validation',
            'status',
            'recommended_suppliers',
            'issuer',
            'delivery_date',
            'budget_estimate',
            'quantity',
            'unit_of_measurement',
            'budget_type',
            'updated_at',
            'created_at',
            'file_url',
            'staff_assigned'
        ]
    
    def create(self, validated_data):
        issuer = validated_data.pop('issuer')
        issuer = Staff.objects.get(user__email=issuer['user']['email'])
        assigned = validated_data.pop('staff_assigned')
        staff_assigned = Staff.objects.get(user__email=assigned['user']['email'])
        instance = Expression(issuer=issuer, **validated_data)
        instance.save()
        return instance
    
    def update(self, instance, validated_data):
        try:
            staff = validated_data.pop('staff_assigned')
            staff_assigned = Staff.objects.get(user__email=staff['user']['email'])
            instance.staff_assigned = staff_assigned
            instance.save()
            return instance
        except Exception as ex:
            return ex

    
        