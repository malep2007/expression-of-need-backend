from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from user.models import Staff
from expression.models import Expression
from base_test import BaseTestCase

import json


class ExpressionTestCase(BaseTestCase):
    def test_users_can_view_expressions(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.get('/expression/')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_users_can_create_expression(self):
        expression = {
            "reason": "cabinet for safe storage of equipment",
            "location": "kampala",
            "what": "Storage Cabinet",
            "recommended_suppliers": "Nina Interiors",
            "issuer": {
                "user": {
                    "email": "nasser.kiti@totalenergies.com"
                }
            },
            "delivery_date": "2021-10-06",
            "budget_estimate": 2000000,
            "budget_type": "OPEX",
            "quantity": 20,
            "unit_of_measurement": "unit",
            "file": "https://www.google.co.ug",
            "staff_assigned": {
                "user": {
                    "email": "nasser.kiti@totalenergies.com"
                }
            },
        }
        response = self.client.post('/expression/create/', data=expression)
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # now we can authenticate the request
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.post('/expression/create/', data=expression)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_update_an_expression(self):
        self.client.credentials()
        data = {
            "id": 1,
            "staff_assigned": {
                "user": {
                    "email": "nasser.kiti@totalenergies.com"
                }
            }
        }
        response = self.client.put('/expression/', data=data)
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.put('/expression/', data=data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
