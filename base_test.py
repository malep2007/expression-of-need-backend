from django.test import TestCase
from rest_framework.test import APIRequestFactory, APIClient
from rest_framework import status
from django.contrib.auth.models import User, Group, Permission
from user.models import Staff
from expression.models import Expression
from rest_framework.authtoken.models import Token


class BaseTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='nkiti', password='123test', email='nasser.kiti@totalenergies.com')
        self.staff = Staff.objects.create(
            user=self.user, department="Strategy and Performance", job_title="Business Solutions Officer")
        self.token = Token.objects.get(user=self.user)
        expression = {
            "reason": "cabinet for safe storage of equipment",
            "location": "kampala",
            "what": "Storage Cabinet",
            "recommended_suppliers": "Nina Interiors",
            "delivery_date": "2021-10-06",
            "budget_estimate": 2000000,
            "budget_type": "OPEX",
            "quantity": 20,
            "unit_of_measurement": "unit",
            "file_url": "https://www.google.co.ug",
        }
        self.expression = Expression.objects.create(**expression, issuer=self.staff, staff_assigned=self.staff)
        self.procurement = Group.objects.create(name='Procurement')
        self.view_permission = Permission.objects.get(name='Can view expression')
        self.add_permission = Permission.objects.get(name='Can add expression')
        self.user.user_permissions.add(self.view_permission)
        self.user.user_permissions.add(self.add_permission)
        self.client = APIClient()
        
            
