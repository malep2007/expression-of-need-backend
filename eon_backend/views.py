from django.shortcuts import render

def index(request):
    return render(request, 'eon_backend/index.html', context={'message': "Template system works"})