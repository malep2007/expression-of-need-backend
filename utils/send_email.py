from mailjet_rest import Client
import os 

api_key = os.environ.get('MAILJET_API_KEY')
api_secret = os.environ.get('MAILJET_API_SECRET')
mailjet = Client(
    auth=(api_key, api_secret),
    version='v3.1'
)

data = {
    'Messages': [
        {
            "From": {
                "Email": "ephraim.malinga@gmail.com",
                "Name": "Digital and Business Solutions Team"
            },
            "To": [
                {
                    "Email": "rehomax893@cyadp.com",
                    "Name": "Ephraim Malinga"
                }
            ],
            "Subject": "Greetings from Mailjet.",
            "TextPart": "My first Mailjet email",
            "HtmlPart": "<h3>Dear passsenger 1, welcome to a <a href='#'>MailJet</a></h3>",
            "CustomID": "AppGettingStartedTest"
        }
    ]
}

def send_new_account_email(to_emails: list, template_name, messageId, subject, variables):
    reset_password_data = {
            "Messages": [
                {
                    "From": {
                        "Email": os.environ.get('MAILJET_FROM_EMAIL'),
                        "Name": os.environ.get('MAILJET_NAME_EMAIL')
                    },
                    "To": to_emails,
                    "Subject": subject,
                    "HtmlPart": "<p>Dear {{var:name:\"\"}},</p><p>Your account has been created. Your temporary password is {{var:password:\"\"}} and username is {{var:username:\"\"}}. Login and use this <a href='{{var:link:\"\"}}'>link</a>. to change your password.</p><p>If this email was received in error please ignore it.</p><hr>For support, please reach out to the Business Solutions team",
                    "CustomID": messageId,
                    "TemplateLanguage": True,
                    "Variables": variables
                }
            ]
        }

    try:
        result = mailjet.send.create(data=reset_password_data)
        print(result.status_code)
        print(result.json())
    except Exception as ex:
        print(ex)    



def send_reset_email(to_emails: list, template_name, messageId, subject, variables):
    reset_password_data = {
        "Messages": [
            {
                "From": {
                    "Email": os.environ.get('MAILJET_FROM_EMAIL'),
                    "Name": os.environ.get('MAILJET_NAME_EMAIL')
                },
                "To": to_emails,
                "Subject": subject,
                "HtmlPart": "<p>Dear {{var:name:\"\"}},</p><p>Please reset your password using this <a href='{{var:link:\"\"}}'>link</a>.</p><p>If this email was received in error please ignore it.</p><hr>For support, please reach out to the Business Solutions team",
                "CustomID": messageId,
                "TemplateLanguage": True,
                "Variables": variables
            }
        ]
    }

    try:
        result = mailjet.send.create(data=reset_password_data)
        print(result.status_code)
        print(result.json())
    except Exception as ex:
        print(ex)
