from django.conf import settings
from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from rest_framework.authtoken.models import Token

from django.contrib.auth.models import User

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

class Staff(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    job_title = models.CharField(max_length=50, null=True)
    department = models.CharField(max_length=50, null=True)
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        if self.user:
            return f'{self.user.username}'
