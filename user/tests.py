from base_test import BaseTestCase

class UserTestCase(BaseTestCase):
    def test_register_user(self):
        """
        Tests to see if a user can be registered by the system
        """
        user_data = {
            "user": {
                "first_name": "test",
                "last_name": "user",
                "email": "test.user@totalenergies.com"
            },
            "department": "Strategy and Performance",
            "job_title": "Digital and Business Solutions"
        }
        response = self.client.post('/users/add/', user_data, format='json')
        self.assertEquals(response.status_code, 201)

    def test_authentication_user(self):
        """
        Tests to see if a user can be authenticated
        """
        data = {
                "username": "nkiti",
                "password": "123test"
            }
        response = self.client.post('/users/authenticate/', data, format='json')
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, self.token)
