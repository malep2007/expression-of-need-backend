from typing import List
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns


from .views import AuthenticateUsers, CreateStaffView, GetStaffByPkDetailView, GetStaffByTokenDetailView, ListStaffView, ListProcurementStaff

urlpatterns = format_suffix_patterns([
    path('<int:pk>/', GetStaffByPkDetailView.as_view()),
    path('get/', GetStaffByTokenDetailView.as_view()),
    path('add/', CreateStaffView.as_view()),
    path('authenticate/', AuthenticateUsers.as_view()),
    path('procurement/get/', ListProcurementStaff.as_view())
])