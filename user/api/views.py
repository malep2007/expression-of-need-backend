from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from user.serializers import StaffSerializer, UserSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions, status

from user.models import Staff

class ListStaffView(APIView):
    """
    View to view all staff members
    """
    authentication_classes = [authentication.TokenAuthentication]
    permissions_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get(self, request, format=None):
        all_staff = Staff.objects.all()
        serializer = StaffSerializer(all_staff, many=True)
        return Response(serializer.data)


class ListProcurementStaff(APIView):
    """
    View that helps to list all staff in procurement Group
    """
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get(self, request, format=None):
        procurement = Group.objects.get(name="Procurement")
        users = procurement.user_set.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    

class CreateStaffView(APIView):
    """
    View used to create a staff profile
    """
    def post(self, request, format=None):
        serializer = StaffSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GetStaffByPkDetailView(APIView):
    """
    View used to get details of a staff
    """
    def get(self, request, pk):
        try:
            staff = Staff.objects.get(pk=pk)
            if staff is not None:
                serializer = StaffSerializer(staff)
                return Response(serializer.data, status=status.HTTP_302_FOUND)
        except Exception as ex:
            return Response({"error":f"{ex}"}, status=status.HTTP_200_OK)

class GetStaffByTokenDetailView(APIView):
    """
    View used to get details of an authenticated user
    """
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    def get(self, request):
        if request.user:
            user = Staff.objects.get(user__username=request.user)
            serializer = StaffSerializer(user)
            return Response(serializer.data, status.HTTP_200_OK)
        else:
            return Response({"message": "no user authenticated"}, status=status.HTTP_401_UNAUTHORIZED)


class AuthenticateUsers(ObtainAuthToken):
    """
    Authentication class to retrieve user tokens 
    """
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, 
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email
        })
