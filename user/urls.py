from typing import List
from django.urls import path 
from django.contrib.auth import views as auth_views

from user.views import  login_view, reset_password, forgot_password, change_password, register_user, logout_user

# define the url patters to use

urlpatterns = [
    path('login/', login_view, name='login'),
    path('logout/', logout_user, name='logout'),
    path('reset/<token_key>/', reset_password, name='reset-password'),
    path('forgot-password/', forgot_password, name='forgot-password'),
    path('change-password/', change_password, name='change-password'),
    path('register/', register_user, name='register')
]