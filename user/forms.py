from django.contrib.auth.forms import AuthenticationForm, UsernameField, PasswordChangeForm, SetPasswordForm, UserCreationForm
from django.contrib.auth.models import User
from django.forms.models import modelformset_factory
from django.contrib.auth.hashers import make_password
from django.urls import reverse
from django import forms
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.utils.crypto import get_random_string
import re

from utils.send_email import send_new_account_email
from user.models import Staff

class LoginForm(AuthenticationForm):
    """
    LoginForm class: 
    Subclass that provides the fields for login details
    """
    # Override the default fields in the AuthenticationForm class to add custom css and other attributes
    username = UsernameField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control form-control-user',
                'placeholder': 'Enter your username',
                'id': 'exampleInputEmail'
            }
        )
    ) 

    password = forms.CharField(
        label="Password",
        strip=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control form-control-user',
                'type': 'password',
                'id':'exampleInputPassword'
            }
        )
    )

class ChangePasswordForm(PasswordChangeForm):
    """
    ChangePasswordForm class: 
    Subclass that provides the fields for resetting a user's password details
    """
    # override the default fields
    old_password = forms.CharField(
        label="Old password",
        strip=False,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control form-control-user',
                'placeholder': 'Enter your old password',
                'id': 'exampleInputEmail'
            }
        ),
    )

    new_password1 = forms.CharField(
        label="New Password 1",
        strip=False,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control form-control-user',
                'placeholder': 'Enter your new password',
                'id': 'exampleInputEmail'
            }
        ),
    )

    new_password2 = forms.CharField(
        label="New Password 2",
        strip=False,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control form-control-user',
                'placeholder': 'Enter your new password again',
                'id': 'exampleInputEmail'
            }
        ),
    )



class ForgotPasswordForm(forms.Form):
    email = forms.CharField(
        required=True,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control form-control-user',
                'placeholder': 'Please enter your email address',
                'id': 'exampleInputEmail'
            }
        )
    )

    def is_total_email(self, email):
        match = re.search(r'\w+@+totalenergies+.com', email)
        if not match:
            raise ValidationError('Your email is wrong!!')

    def clean_email(self):
        data = self.cleaned_data['email']
        self.is_total_email(data)
        return data



class ResetPasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(
        label='Insert Password',
        strip=False,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control form-control-user',
                'id': 'exampleInputEmail'
            }
        )
    )

    new_password2 = forms.CharField(
        label='Enter your password again', 
        strip=False,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control form-control-user',
                'id': 'exampleInputEmail'
            }
        )
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class CreateStaffAccountForm(forms.Form):
    username = forms.CharField(
            label='Username',
            widget=forms.TextInput(
                attrs={
                    'class': 'form-control form-control-user',
                    'id': 'exampleInputEmail',
                    'placeholder': 'Enter your username'
                }
            )
        )

    first_name = forms.CharField(
        label='First Name',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control form-control-user',
                'id': 'exampleInputEmail',
                'placeholder': 'Enter your first name'
            }
        )
    )

    last_name = forms.CharField(
        label='Last Name',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control form-control-user',
                'id': 'exampleInputEmail',
                'placeholder': 'Enter your last name'
            }
        )
    )

    email = forms.EmailField(
        label='email',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control form-control-user',
                'id': 'exampleInputEmail',
                'placeholder': 'Enter valid email address'
            }
        )
    )

    
    job_title = forms.CharField(
        label='Job Title',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control form-control-user',
                'id': 'exampleInputEmail',
                'placeholder': 'Enter Job Title'
            }
        )
    )

    department = forms.CharField(
        label='Department',
        strip=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control form-control-user',
                'id': 'exampleInputEmail',
                'placeholder': 'Enter name of dedpartment'
            }
        )
    )


    def save(self):
        try:
            raw_password = get_random_string(length=8)
            user,created = User.objects.get_or_create(
                username=self.cleaned_data['username'],
                first_name=self.cleaned_data['first_name'],
                last_name=self.cleaned_data['last_name'],
                email=self.cleaned_data['email'],
                password=make_password(raw_password)
            )
            
            staff = Staff.objects.create(
                user=user,
                department=self.cleaned_data['department'],
                job_title=self.cleaned_data['job_title']
            )
            send_new_account_email(
                to_emails=[
                    {
                        "Email": user.email,
                        "Name": user.get_full_name()
                    }
                ],
                template_name='user/email/new_account_email.html',
                messageId='NewAccountEmail${user.pk}',
                subject='Your Account has been created',
                variables={
                    "name": user.first_name,
                    "password": raw_password,
                    "username": user.username,
                    "link": reverse('change-password')
                }
            )
        except Exception as ex:
            raise ValidationError(ex)


