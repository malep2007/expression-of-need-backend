"""
Serializer module for the user app
"""
from django.contrib.auth.models import User
from rest_framework import serializers, permissions
from user.models import Staff


class UserSerializer(serializers.ModelSerializer):
    """
    UserSerializer class to handle user creation
    """

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

class StaffSerializer(serializers.ModelSerializer):
    """
    StaffSerializer to handle creation of Staff data
    """
    user = UserSerializer()
    class Meta:
        model = Staff
        fields = ['user', 'department', 'job_title']
        
    def create(self, validated_data):
        user = validated_data.pop('user')
        first_name = user['first_name']
        last_name = user['last_name']
        username = f'{first_name[0].lower()}{last_name.lower()}'
        department = validated_data.pop('department')
        job_title = validated_data.pop('job_title')
        user = User.objects.create_user(**user, username=username, password="123test")
        staff_record = Staff.objects.create(user=user, department=department, job_title=job_title)
        return staff_record
