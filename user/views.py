from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.contrib.auth import views as auth_views, authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.views import LoginView
from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework.authtoken.models import Token

from user.forms import LoginForm, ChangePasswordForm, ForgotPasswordForm, ResetPasswordForm, CreateStaffAccountForm
from utils.send_email import send_reset_email


@login_required
def change_password(request):
    form = ChangePasswordForm(user=request.user)
    if request.method == 'POST':
        form = ChangePasswordForm(data=request.POST)
        if form.is_valid():
            return HttpResponse('Password Reset')
        else:
            return render(request, template_name='user/change_password.html', context={'form': form})
    else:
        return render(request, template_name='user/change_password.html', context={'form':form})

def reset_password(request, token_key):
    form = ResetPasswordForm(user=request.user)
    try:
        token = Token.objects.get(key=token_key)
        user = token.user
        if request.method == 'POST':
            form = ResetPasswordForm(user=user, data=request.POST)
            if form.is_valid():
                user.set_password(form.cleaned_data['new_password1'])
                user.save()
                return redirect(to='/users/login/')
            else:
                return render(request, template_name='user/reset_password.html', context={'form':form})
        else:
            return render(request, template_name='user/reset_password.html', context={'form':form})
    except Exception as ex:
        return render(request, template_name='user/reset_password.html', context={'form':form, 'error':ex})


def forgot_password(request):
    form = ForgotPasswordForm()
    if request.method == 'POST':
        form = ForgotPasswordForm(data=request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            try:
                user = User.objects.get(email=email)
                token, created = Token.objects.get_or_create(user=user) # to be used as parameter to pass to reset link
                if not user:
                    return render(request, template_name='user/forgot_password.html', context={'form':form, 'error': "user does not exist"})
                else:
                    send_reset_email(
                        to_emails=[
                            {
                                "Email": "rehomax893@cyadp.com", # form.fields['email'],
                                "Name": "Reset Password"
                            }
                        ], 
                        template_name="user/email/reset_password_email", 
                        messageId="AppResetPassword", 
                        subject="ExpressRQ - Reset Pasword",
                        variables={
                            "name": user.first_name,
                            "link": reverse('reset-password', args=(token,))
                        }
                    )
                    return redirect(to='/users/login/')
            except Exception as ex:
                return render(request, template_name='user/forgot_password.html', context={'form':form, 'error':ex})
        else:
            return render(request, template_name='user/forgot_password.html', context={'form':form})
    return render(request, template_name='user/forgot_password.html', context={'form':form})

def login_view(request):
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
            if user is not None:
                login(request, user)
                return redirect(to='/expression/')
            else:
                return render(request, template_name='user/login.html', context={'errors': 'user does not exist'})
        else:
            errors = form.errors
            return render(request, template_name='user/login.html', context={'form':form, 'errors': errors})
    else:
        form = LoginForm() 
        return render(request, template_name='user/login.html', context={'form': form})

@permission_required('auth.add_user') # permission group ensures that only admins/ those with this can add/register users
def register_user(request):
    form = CreateStaffAccountForm()
    if request.method == 'POST':
        form = CreateStaffAccountForm(data=request.POST)
        if form.is_valid():
            form.save()
            return render(request, template_name='user/register_user.html', context={'form':form})
        else:
            return render(request, template_name='user/register_user.html', context={'form':form})
    else:
        return render(request, template_name='user/register_user.html', context={'form': form})

@login_required
def logout_user(request):
    logout(request)
    return redirect(to='/users/login/')


